﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project2;

namespace Project1
{
    class Program
    {
        static Random rnd = new Random();
        static void Main(string[] args)
        {
            //double r = rnd.NextDouble();
            //int year = DateTime.Now.Year;
            //Class2 obj = new Class2();
            //obj.Index = 8;
            //Console.WriteLine(obj.Index);
            ////obj.Measurement = 2.5;
            //Console.WriteLine(obj.Measurement);

            //int[,,] arr3D = new int[2, 3, 4];
            //Console.WriteLine(arr3D[1,2,0]);

            //int[][] arrOfArrays; // null
            //arrOfArrays = new int[3][]; // [null,null,null]
            //for (int i = 0; i < 3; ++i)
            //    arrOfArrays[i] = new int[i + 1];
            //// [[0],[0,0],[0,0,0]]

            //Class2 obj3 = new Class2();
            ////...
            //obj3 = null;

            printNumbers("my numbers", 1, 2, 3, 4, 5);
            printNumbers("nothing");
        }

        static void printNumbers(string prompt, params int[] numbers)
        {
            Console.Write(prompt + ":");
            for (int i = 0; i < numbers.Length; ++i)
                Console.Write(" " + numbers[i]);
            Console.WriteLine();
        }
    }
}
