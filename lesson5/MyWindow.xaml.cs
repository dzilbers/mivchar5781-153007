﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace lesson5
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            //Button1.Click += Button1_Click;
            this.Loaded += MainWindow_Loaded;
        }

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            lbJacobChildren.Background = Brushes.Aqua;
        }

        private void Button1_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("האם לסגור את החלון", "AAA",
                MessageBoxButton.YesNoCancel,
                MessageBoxImage.Question,
                MessageBoxResult.No,
                MessageBoxOptions.RightAlign | MessageBoxOptions.RtlReading);
            if (result == MessageBoxResult.Yes)
                this.Close();
            else
            {
                ((Button)sender).Content = "Try me again";
                if (newButton == null)
                {
                    newButton = new Button();
                    newButton.Content = "BLABLAB";
                    ((Grid)Button1.Parent).Children.Add(newButton);
                }
            }
        }

        Button newButton;

        private void MyClick(object sender, RoutedEventArgs e)
        {

        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int busNo = 5;
            labels[busNo].Background = Brushes.Red;
        }

        Dictionary<int, Label> labels;
        // labels.Clear(); listbox.datacontext = somelist; 
        private void lbl1_Loaded(object sender, RoutedEventArgs e)
        {
            Label me = sender as Label;
            labels.Add(int.Parse(me.Tag as String), me);
        }
    }
}
