﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson3
{
    interface IDoing
    {
        string DoSomething(int value);
    }

    class MyColl : IEnumerable //<int>
    {
        int[] arr = { 1, 2, 3 };
        //public IEnumerator<int> GetEnumerator() => new MyEnumerator(this);
        public IEnumerator GetEnumerator() => new MyEnumerator(this); //GetEnumerator();

        private class MyEnumerator : IEnumerator //<int>
        {
            private MyColl myColl;
            private int counter = -1;
            internal MyEnumerator(MyColl myColl) => this.myColl = myColl;
            //public int Current => myColl.arr[counter];
            public object Current => myColl.arr[counter]; // Current;
            // public void Dispose() { }
            public bool MoveNext() => ++counter < myColl.arr.Length;
            public void Reset() => throw new InvalidOperationException();
        }
    }

    class MyColl3 : IEnumerable
    {
        int[] arr = { 1, 2, 3 };
        public IEnumerator GetEnumerator() => arr.GetEnumerator();
    }

    class MyColl4 : IEnumerable<int>
    {
        int[] arr = { 1, 2, 3 };
        public IEnumerator<int> GetEnumerator()
        {
            foreach (var num in arr)
                yield return num;
        }
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }

    class MyColl2 : IEnumerable<int>, IEnumerator<int>
    {
        int[] arr = { 1, 2, 3 };
        public IEnumerator<int> GetEnumerator() { counter = -1; return this; }
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        private int counter;
        public int Current => arr[counter];
        object IEnumerator.Current => Current;
        public void Dispose() { }
        public bool MoveNext() => ++counter < arr.Length;
        public void Reset() => throw new InvalidOperationException();
    }

    class Program
    {
        public static IEnumerable<int> Fibonacci()
        {
            int n1 = 0, n2 = 1;
            yield return n1;
            yield return n2;
            while (true)
            {
                int temp = n1 + n2;
                n1 = n2;
                n2 = temp;
                yield return temp;
            }
        }

        static void Main(string[] args)
        {
            new Derived();
            //Derived2[] derives = new Derived2[10];

            //    Array.Sort(derives);

            //    int i = 0, j = 0;
            //    if (derives[i].CompareTo(derives[j]) > 0)
            //    {
            //        ///swap(...)
            //    }

            //BaseClass bb = new Derived2(); // המרה כלפי מעלה
            //                               // ...
            //Derived dd;
            //    if (bb is Derived)
            //        dd = (Derived) bb; // if it is not good for Derived - InvalidCastException will be thrown
            //    else
            //        dd = null;

            //    dd = bb as Derived;


            //    foreach (var n in Fibonacci())
            //        if (n > 100) break;
            //        else Console.Write(" " + n);

            //    //var allFibs = Fibonacci();

            //    Console.WriteLine();
            //    //Derived d = new Derived();
            //int[] numbers = { 1, 2, 3, 4, 5, 6 };
            //MyColl4 coll = new MyColl4();

            MyColl numbers = new MyColl();
            var it = numbers.GetEnumerator();
            while (it.MoveNext())
            {
                int num = (int)it.Current;
                Console.Write(" " + num);
            }
            Console.WriteLine();

            foreach (int num in numbers)
            {
                Console.Write(" " + num);
                
            }
            Console.WriteLine();

            //foreach (var num in coll)
            //    Console.Write(" " + num);
            //Console.WriteLine();

            //MyColl coll2 = new MyColl();
            //foreach (var num1 in coll2)
            //    foreach (var num2 in coll2)
            //        Console.WriteLine("" + num1 + ":" + num2);

            Console.ReadLine();
        }
    }

    class Derived2 : BaseClass, IDoing, IComparable<Derived2>
    {
        bool stam;

        public void foo()
        {
            Console.WriteLine(stam); // this.stam
            Console.WriteLine(base.stam); // BaseClass.stam
                                          // Console.WriteLine(base.base.ToString()); // ERROR
        }

        public string DoSomething(int value)
        {
            return "" + value;
        }

        public int CompareTo(Derived2 obj)
        {
            if (stam == obj.stam) return 0;
            else if (stam) return 1;
            else return -1;
        }

        public Derived2() : base(9) { }
    }

    class BaseClass
    {

        public BaseClass()
        {
            Console.WriteLine("Base.Instance.Constructor");
            //this.m_Field3 = new Tracker("Base.Instance.Field3");
            //this.Virtual();
            //Console.WriteLine(base.ToString()); // Object.ToString()
        }
        static BaseClass()
        {
            Console.WriteLine("Base.Static.Constructor");
        }
        private Tracker m_Field1 = new Tracker("Base.Instance.Field1");
        private Tracker m_Field2 = new Tracker("Base.Instance.Field2");
        //private Tracker m_Field3;
        protected int stam;
        static private Tracker s_Field1 = new Tracker("Base.Static.Field1");
        //virtual public void Virtual()
        //{
        //    Console.WriteLine("Base.Instance.Virtual");
        //}
        public BaseClass(int num) { }

        public override string ToString() { return "WOW"; }
    }
    class Derived : BaseClass
    {
        public Derived()
        {
            Console.WriteLine("Derived.Instance.Constructor");
            //this.m_Field3 = new Tracker("Derived.Instance.Field3");
        }
        public Derived(int num) : base(num) { }
        static Derived()
        {
            Console.WriteLine("Derived.Static.Constructor");
        }
        private Tracker m_Field1 = new Tracker("Derived.Instance.Field1");
        private Tracker m_Field2 = new Tracker("Derived.Instance.Field2");
        //private Tracker m_Field3;
        static private Tracker s_Field1 = new Tracker("Derived.Static.Field1");
        static private Tracker s_Field2 = new Tracker("Derived.Static.Field2");
        //override public void Virtual()
        //{
        //    Console.WriteLine("Derived.Instance.Virtual");
        //}
    }
    class Tracker
    {
        public Tracker(string text)
        {
            Console.WriteLine(text);
        }
    }
}

