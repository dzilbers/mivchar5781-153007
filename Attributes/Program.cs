﻿using System;
using System.Diagnostics;

namespace Attributes
{
    class Person
    {
        public int Id;
        public string FirstName;
        public string LastName;
        string more = "the best";

        [Obsolete("Try to stop using this function - need to use better()", false)]
        public void Foo()
        {
            Console.WriteLine(LastName + "-dude");
        }

        public void Better()
        {
            Console.WriteLine(LastName + " the frum");
        }
        public override string ToString()
        {
            return $"Id = {Id}, First Name = {FirstName}, Last Name = {LastName}";
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Person p1 = new Person() { Id = 123, FirstName = "Beni", LastName = "Yeled-Tov" };
            p1.Foo();
            Console.WriteLine(p1);
        }
    }
}
