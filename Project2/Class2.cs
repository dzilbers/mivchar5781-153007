﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project2
{
    public class Class2
    {
        int index;
        public int Index
        {
            //get { return index; }
            get => index;
            //set { index = value >= 0 ? value : 0; }
            set => index = value >= 0 ? value : 0;
        }

        //=================================================================================

        public double Measurement { get; private set; }
        // private double secretField;
        // public double Measurement { get => secretField; private set => secreField = value; }

        public Class2()
        {
            Measurement = 3.14;
        }

        public DateTime Today { get => DateTime.Now; }
        
    }
}
