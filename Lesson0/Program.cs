﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace Lesson0
{
    class Program
    {
        static void Main(string[] args)
        {
            int num1;
            Class1 o1 = new Class1();
            Levi.Class1 o2 = new Levi.Class1();
            Console.WriteLine("Program");

            MyStruct strct = new MyStruct(1, 2);
            strct.f4 = 8;
            strct.f1 = 1;
            strct.f2 = 1;
            int m = strct.F3;

            Console.WriteLine("" + strct + " " + m);

            int[] arr1 = new int[8];
            for (int i = 0; i < arr1.Length; ++i)
                Console.Write(" " + arr1[i]);
            Console.WriteLine();

            int[,,] arr2 = new int[2, 3, 4];

            int[][] arr3 = new int[5][]; // 5 X null
            for (int i = 0; i < arr3.Length; ++i)
                arr3[i] = new int[i + 1];

            int[] arr4 = { 1, 2, 3, 4, 5 };

            var dani = 2;
            f1(ref dani);
            Console.WriteLine(dani);

            MyWL(1, 2, 3, 4, 5, 6);
            MyWL();

            Console.WriteLine(f2(y: 4, x: 8));
        }

        static void f1(ref int n) { n = 98; }

        static void MyWL(params int[] numbers)
        {
            foreach (var n in numbers)
                Console.WriteLine(n);
        }

        /// <summary>
        /// This function will try to divide two numbers.
        /// If the denominator will be zero - the function will swear.
        /// </summary>
        /// <param name="x">numerator</param>
        /// <param name="y">denominator</param>
        /// <returns>division result if the denominator is not zero
        /// List&lt;T&gt;</returns>
        static int f2(int x, int y) => x / y;
    }

    /// <summary>
    /// It's the best class anyone produced in the whole history
    /// of hi-tech
    /// </summary>
    class Class1 { }

    struct MyStruct
    {
        static int counter;
        public int f1, f2;
        int f3;
        public int F3
        {
            get => f3; // get { .... return ...; }
            //private set => f3 = value; // set { f3 = value; } 
        }

        public DateTime Today
        {
            get => DateTime.Now;
        }

        public int F5 { get; private set; }
        // int f5;
        // get => f5;
        // set => f5 = value;

        public int f4;
        static MyStruct()
        {
            counter = 1;
        }
        public MyStruct(int n1, int n2)
        {
            f1 = n1;
            f2 = n2;
            f4 = 9;
            f3 = 0;
            //F3 = 3;
            F5 = 1;
        }

        public override string ToString()
        {
            return "" + f1 + " " + f2 + " " + f3 + " " + f4;
        }
    }
}

namespace Levi
{
    class Class1
    {
        static int counter = 1;
        int color;
        Class1 obj; // null
        Class1 obj2;

        public Class1(int c)
        {
            color = default(int);
        }
        public Class1() { }

        static Class1()
        {
            counter = 1;
        }
    }


}
