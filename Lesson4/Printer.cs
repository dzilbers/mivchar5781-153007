﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson4
{
    public delegate void PrinterEventHandler();
    class PrinterEventArgs : EventArgs
    {
        public readonly int Pages;
        public bool Done = false;
        public PrinterEventArgs(int pages) => this.Pages = pages;
    }
    class Printer
    {
        int pageCount = 20;

        public event EventHandler<PrinterEventArgs> PageOver;
        void handlePageOver(int pages)
            => PageOver(this, new PrinterEventArgs(pages));

        public void Print(int pages)
        {
            if (pages <= pageCount)
                pageCount -= pages;
            else
            {
                handlePageOver(pages - pageCount);
                pageCount = 0;
            }

        }
    }
}
