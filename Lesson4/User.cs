﻿using System;
using System.Collections.Generic;
using System.Deployment.Internal;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Lesson4
{
    class User
    {
        static Random rnd = new Random();
        IPAddress ip;
        int id;
        Printer myPrinter;
        public User(Printer printer, int userId)
        {
            id = userId;
            myPrinter = printer;
            printer.PageOver += myPageOver;
        }

        private void myPageOver(object sender, PrinterEventArgs pEv)
        {
            if (sender != myPrinter) return;
            //PrinterEventArgs pEv = (PrinterEventArgs)ev;
            if (pEv.Done) return;
            if (rnd.NextDouble() < 0.5)
            {
                pEv.Done = true;
                Console.WriteLine($"{id} - Run and bring new paper urgently! Failed to print {pEv.Pages} pages");
            }
            else
                Console.WriteLine($"{id} - I don't care - it's not my business...");
        }
    }
}
