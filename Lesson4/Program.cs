﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Lesson4
{
    public delegate int MyDelegate(int counter);

    class Program
    {

        #region delegate doIt
        static MyDelegate doIt = null;

        static int func1(int ctr)
        {
            Console.WriteLine("func1");
            return 1;
        }
        static int func2(int ctr)
        {
            Console.WriteLine("func2");
            return 2;
        }
        static int func3(int ctr)
        {
            Console.WriteLine("func3");
            return 3;
        }
        #endregion

        static void Main(string[] args)
        {
            #region anonimous object
            Console.WriteLine("======================================");
            PrintInfo("", typeof(MyClass));
            Console.WriteLine("======================================");
            var lecturer = new { Id = 123456, Name = "Mordi Hagiz" };
            PrintInfo("", lecturer.GetType());
            Console.WriteLine("Lecturer: " + lecturer);
            Console.WriteLine("======================================");
            //int id = new MyClass().get_Id();
            #endregion

            #region extension function
            string str = "Efraim";
            // str.ToInt();
            //int i = "12345".ToInt();
            //1.ToStringProperty();
            //new DateTime(2020, 4, 5, 12, 36, 7).ToStringProperty();
            #endregion

            #region delegate
            //doIt = null;
            //doIt = func1;
            //doIt += func2;
            //Console.WriteLine("====================");
            //Console.WriteLine(doIt(10));

            //Console.WriteLine("====================");
            //foreach (MyDelegate d in doIt.GetInvocationList())
            //{
            //    Console.WriteLine(d.Method);
            //    Console.WriteLine(d(10));
            //}
            //Console.WriteLine("====================");
            //Console.WriteLine(((MyDelegate)doIt.GetInvocationList()[0])(11));
            #endregion

            #region Predicate
            //List<int> nums = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            //foreach (int num in nums.FindAll(even))
            //    Console.Write($"{num};");
            //Console.WriteLine();
            #endregion

            #region Printer event
            //Printer printer = new Printer();
            //printer.PageOver(); // blocked for event delegate field
            //User user1 = new User(printer, 1);
            //User user2 = new User(printer, 2);

            //printer.Print(30);
            #endregion

            #region dynamic trial
            //Console.WriteLine("Press any key to proceed to dynamic trial");
            //Console.ReadKey();

            //dynamic trial = new MyClass();
            //trial.Id = 89123;
            //trial.Name = "Yossi Cohen";
            //Console.WriteLine(trial.ToString());

            //trial = nums.MyFindAll(x => x % 3 == 0);
            //foreach (int num in trial)
            //    Console.Write($"{num};");
            //Console.WriteLine();
            #endregion
        }

        static void PrintInfo(string suffix, Type type)
        {
            Console.WriteLine(suffix + "Type name: " + type.Name);
            Console.Write(suffix + "Base");
            if (type.BaseType == null)
                Console.WriteLine(" name: None");
            else
            {
                Console.WriteLine(":");
                PrintInfo(suffix + "  ", type.BaseType);
            }
            Console.WriteLine(suffix + "Member Info:");
            MemberInfo[] members = type.GetMembers(BindingFlags.Public |
                BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.Instance);
            foreach(var item in members)
                Console.WriteLine(suffix +
                    "name: {0,-15} type: {2,-11} in: {1}",
                    item.Name, item.DeclaringType, item.MemberType);
        }

        static bool even(int n) => n % 2 == 0;
    }

    class MyClass
    {
        public int Id { get; set; }

        private string name;
        public string Name { get => name; set => name = value; }

        public int Age = 18;

        public int Stam { get => 0; }

        public override string ToString()
        {
            return $"{{ Id == {Id}, Name = \"{Name}\", Age = {Age}, Stam = {Stam}}}";
        }
    
    }

    static class MyTools
    {
        public static int ToInt(this string str)
        {
            int temp;
            return int.TryParse(str, out temp) ? temp : int.MaxValue; 
        }

        public static void ToStringProperty<T>(this T obj)
        {
            string str = "";
            foreach (var item in obj.GetType().GetProperties())
                str += $"\n{item.Name}: {item.GetValue(obj)} ({item.PropertyType.Name})";
            Console.WriteLine(str);
        }

        public static List<T> MyFindAll<T>(this List<T> coll, Predicate<T> pred)
        {
            int y = 8;
            List<T> newColl = new List<T>();
            foreach (var obj in coll)
                if (pred(obj))
                    newColl.Add(obj);
            return newColl;
        }


    }
}
