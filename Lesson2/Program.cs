﻿using System;

namespace Lesson2
{
    /// <summary>
    /// The best program in the word
    /// </summary>
    class Program
    {
        static int glob;

        static void Main(string[] args)
        {
            Class1 obj = new Class1();
            int number = 2;
            Class1 obj2 = obj;
            int calc = 2 + 3 + 3 + 3 + 3 + 3 + 3 + 3 + 3 + 3 + 3 + 3 + 3 + 3 + 3 
                + 3 + 3 + 3 + 3 + 3 + 3 + 3 + 3
                + 3 + 3 + 3 + 3 + 3 + 3 + 3 + 3 + 3;
            f(num: number,
                o: ref obj);
            Console.WriteLine($"Obj1 field {obj.f1}, number {number}");
            //Console.WriteLine($"Obj2 field {obj2.f1}");
        }

        /// <summary>
        /// What a function!!!
        /// </summary>
        /// <param name="o">very good object</param>
        /// <param name="num">nice number</param>
        /// <returns>just a zero</returns>
        static int f(ref Class1 o, int num = 9)
        {
            //Console.WriteLine($"In function: {o.f1}, {num}");
            o = new Class1();
            o.f1 = 5;
            num = 8;
            glob = 6;
            return 0;
        }
    }

    class Class1
    {
        public int f1 = 0;
    }
}
